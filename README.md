# MUIS transcriptome (trx) analysis code

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.5736115.svg)](https://doi.org/10.5281/zenodo.5736115)
[![CC BY 4.0][cc-by-shield]][cc-by]

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg

_Author_: Wouter A.A. de Steenhuijsen Piters <a href="https://orcid.org/0000-0002-1144-4067" target="orcid.widget" rel="noopener noreferrer" style="vertical-align:top;"><img src="https://orcid.org/sites/default/files/images/orcid_16x16.png" style="width:1em;margin-right:.5em;" alt="ORCID iD icon"></a>  
_Date_: 29-11-2021

This code accompanies the manuscript '_Early-life viral infections are associated with disadvantageous immune and microbiota profiles and recurrent respiratory infections_', which was published in [_Nature Microbiology_](https://www.nature.com/articles/s41564-021-01043-2).

## Description

This repository contains the Rmd-scripts ([`scripts/`](scripts/)) and functions ([`src/`](src/)) used to generate results ([`results/`](results/)) for the Microbiome Utrecht Infant Study (MUIS) transcriptome project. The aim of this project was to assess early-life nasopharyngeal gene expression profiles in the context of the local microbiota and viral infection.

Raw gene expression ([GSE152951](https://www.ncbi.nlm.nih.gov/bioproject/?term=GSE152951)) and microbiota ([PRJNA740120](https://www.ncbi.nlm.nih.gov/bioproject/PRJNA740120/)) data were made publicly available, including minimal participant metadata. Formatted input-files, including full participant metadata  (notably `env_comb.Rdata`) are available upon request if not interfering with running projects (please contact d.bogaert@ed.ac.uk or w.a.a.desteenhuijsenpiters@umcutrecht.nl).  

The code below was ran in R version 4.1.0.

## File description

[`scripts`](scripts/)/

- Data preprocessing  
  - [`1.0_prep_meta.Rmd`](scripts/1.0_prep_meta.Rmd): prepare metadata  
  - [`1.1_prep_ma.Rmd`](scripts/1.1_prep_ma.Rmd) : prepare ma (microarray) data  
  - [`1.2_meta_viral.Rmd`](scripts/1.2_meta_viral.Rmd): prepare viral data  
  - [`1.3_mb_env.Rmd`](scripts/1.3_mb_env.Rmd): prepare mb (microbiota) data  
  - [`1.4_combine.Rmd`](scripts/1.4_combine.Rmd): combine the preprocessed datasets above; output: `env_comb.Rdata`  
- 'Baseline' analyses  
  - [`2.1_baseline_tables.Rmd`](scripts/2.1_baseline_tables.Rmd): creates baseline tables (a.o. Supplementary Table 1)
  - [`2.2_baseline_figures.Rmd`](scripts/2.2_baseline_figures.Rmd): diverse script, includes sequencing descriptives, microbiota clustering analyses and dynamics, early-life microbiota development analyses, figures on viral presence and _lytA_ vs 16S-rRNA-based results  
- Gene expression based analyses  
  - [`3.1_prep_db.Rmd`](scripts/3.1_prep_db.Rmd): script used to curate GO-/Reactome-databases  
  - [`3.2_CEMiTool.Rmd`](scripts/3.2_CEMiTool.Rmd): gene module analyses using CEMiTool
  - [`3.3_survival.Rmd`](scripts/3.3_survival.Rmd): (first) viral detection in relation to module activity (and _vice versa_)  
  - [`3.4_DEGs.Rmd`](scripts/3.4_DEGs.Rmd): differential expression analyses using _limma_  
  - [`3.6_CIBERSORTx.Rmd](scripts/3.6_CIBERSORTx.Rmd)`: assessment of [CIBERSORTx](https://cibersortx.stanford.edu)-results
- 'Global' analyses (dataset-level)  
  - [`4.1_mantel.Rmd`](scripts/4.1_mantel.Rmd): Mantel-tests  
  - [`4.2_stability.Rmd`](scripts/4.2_stability.Rmd): gene expression stability over time  
  - [`4.3_adonis.Rmd`](scripts/4.3_adonis.Rmd): drivers of gene expression/microbiota/viral data (PERMANOVA-tests)  
- Microbiota-based analyses  
  - [`5.1_mb_vir.Rmd`](scripts/5.1_mb_vir.Rmd): analyses on the association between microbiota profiles and viral detection
  - [`5.2_cluster.Rmd`](scripts/5.2_cluster.Rmd): survival analysis linking module activity and microbiota cluster transitions/outcomes
- Integrative analyses   
  - [`6.1_HAllA.Rmd`](scripts/6.1_HAllA.Rmd): [HAllA](https://huttenhower.sph.harvard.edu/halla_legacy/)-analyses on associations between genes and microbiota relative abundance
- Output
  - [`7.2.1_figure_panels.Rmd`](scripts/7.2.1_figure_panels.Rmd): combines all figures selected for publication in a Word-file  
  - [`7.2.2_legends.Rmd`](scripts/7.2.2_legends.Rmd): all legends accompanying figures selected for publication  
  - [`SRA_upload.Rmd`](scripts/SRA_upload.Rmd): script used to prepare data for SRA-upload  
  
The .html-output of these scripts can be found the [`results`](results/)-folder.

## Figure guide

Please find below an overview of all figures included with the manuscript and the scripts used to create those figures.

### Main figures

Fig. 1a-b: [`scripts/2.2_baseline_figures.Rmd`](scripts/2.2_baseline_figures.Rmd)  
Fig. 1c-e: [`scripts/3.4_DEGs.Rmd`](scripts/3.4_DEGs.Rmd)  
Fig. 1f-g: [`scripts/3.2_CEMiTool.Rmd`](scripts/3.2_CEMiTool.Rmd)  
Fig. 1h: [`scripts/3.6_CIBERSORTx.Rmd`](scripts/3.6_CIBERSORTx.Rmd)  

Fig. 2a-b: [`scripts/3.3_survival.Rmd`](scripts/3.3_survival.Rmd)  
Fig. 2c-d: [`scripts/5.1_mb_vir.Rmd`](scripts/5.1_mb_vir.Rmd)  

Fig. 3a: [`scripts/4.2_stability.Rmd`](scripts/4.2_stability.Rmd)  
Fig. 3b-c: [`scripts/3.2_CEMiTool.Rmd`](scripts/3.2_CEMiTool.Rmd)  

Fig. 4a-g: [`scripts/2.2_baseline_figures.Rmd`](scripts/2.2_baseline_figures.Rmd)  

Fig. 5a: [`scripts/5.2_cluster.Rmd`](scripts/5.2_cluster.Rmd)  
Fig. 5b: [`scripts/3.3_survival.Rmd`](scripts/3.3_survival.Rmd)  
Fig. 5c-d: [`scripts/5.2_cluster.Rmd`](scripts/5.2_cluster.Rmd)  

Fig. 6a-f: [`scripts/6.1_HAllA.Rmd`](scripts/6.1_HAllA.Rmd)  
Fig. 6g-h: [`scripts/5.1_mb_vir.Rmd`](5.1_mb_vir.Rmd)   

### Extended Data figures

Extended Data Fig. 1a: [`scripts/4.1_mantel.Rmd`](scripts/4.1_mantel.Rmd)    
Extended Data Fig. 1b: [`scripts/4.3_adonis.Rmd`](scripts/4.3_adonis.Rmd)  

Extended Data Fig. 2: [`scripts/2.2_baseline_figures.Rmd`](scripts/2.2_baseline_figures.Rmd)  

Extended Data Fig. 3: [`scripts/5.1_mb_vir.Rmd`](5.1_mb_vir.Rmd)   

Extended Data Fig. 4: [`scripts/2.2_baseline_figures.Rmd`](scripts/2.2_baseline_figures.Rmd)  

Extended Data Fig. 5: [`scripts/5.2_cluster.Rmd`](scripts/5.2_cluster.Rmd)  

Extended Data Fig. 6a-b: [`scripts/2.2_baseline_figures.Rmd`](scripts/2.2_baseline_figures.Rmd)  

Extended Data Fig. 7: [`scripts/6.1_HAllA.Rmd`](scripts/6.1_HAllA.Rmd)  

Extended Data Fig. 8: [`scripts/5.1_mb_vir.Rmd`](5.1_mb_vir.Rmd)   

Extended Data Fig. 10a-b: [`scripts/2.2_baseline_figures.Rmd`](scripts/2.2_baseline_figures.Rmd)  
