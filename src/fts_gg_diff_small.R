fts_gg_diff_small <- function(fts, interval_length_cutoff = 0, xstart = -Inf, OTU = T, ...) {
  prep <- fts %>%
    mutate(Feature = fct_inorder(Feature) %>% fct_rev(),
           interval_length = `Interval end` - `Interval start`, 
           p_x = `Interval start` + (interval_length/2),
           q_lab = sprintf("%.3f",round(`q value`, 3)),
           lab_type = abs(Area) < 200) %>%
    filter(interval_length > interval_length_cutoff) 
  
  if(OTU) { prep %<>% mutate(Feature = format_OTU(Feature) %>% fct_inorder) }
  
  prep %>%
    ggplot(aes(y = Feature)) +
    geom_segment(aes(x = xstart, xend = Inf, yend = Feature), color = "gray20", linetype = "dashed", size = 0.2) +
    geom_segment(aes(x = `Interval start`, xend = `Interval end`, yend = Feature, colour = Association, size = abs(Area))) +
    geom_text(aes(label = q_lab, x = p_x), size = 3, vjust = if_else(prep$lab_type, -0.7, 0.5), color = if_else(prep$lab_type, "gray20", "white")) +
    scale_x_continuous(expand = c(0,0), limits = c(0, 382)) +
    scale_colour_manual(...) +
    scale_size(limits = c(0, 800)) +
    theme(axis.text.y = element_markdown(), panel.grid.major.x = element_blank(), panel.grid.minor.x = element_blank(),
          panel.grid.major.y = element_blank()) + 
    guides(size = "none") +
    labs(x = "Age (days)", y = "")
}
