---
title: "`r subdir_name`"
author: "Wouter A.A. de Steenhuijsen Piters"
date: "`r Sys.time()`"
---

```{r setup, include=F, message=F}
subdir_name <- "1.4_combine"  # must be same name as the name under which the .Rmd is saved.

library(tidyverse); library(magrittr); library(here); library(glue); library(phyloseq)

# set paths
knitr::opts_knit$set(root.dir=".", aliases=c(h = "fig.height", w = "fig.width", ow = "out.width"))
knitr::opts_chunk$set(dev=c('png', 'pdf'), 
                      fig.path=here("results", "figures", glue("{subdir_name}/")), dpi=300)
theme_set(theme_light()) # global option for ggplot2
```

```{r knit, echo=F, eval=FALSE}
rmarkdown::render(input = here("scripts", str_c(subdir_name, ".Rmd")), output_dir = here("results"))
```

# Load functions

```{r, message = F, warning = F}
source(here::here("src", "utils.R"))
source(here::here("src", "clust_ind_plot.R"))
```

# Loading data

## Microarray data (`_ma`)

```{r}
load(here("data_cleaned", "env_ma.Rdata"))
```

Includes `meta_ma` and `expr` (including meta in `pData`).

## Microbiota data (`_mb`)

```{r}
load(here("data_cleaned", "env_mb.Rdata"))
```

## Viral data (`_vir`)

```{r}
load(here("data_cleaned", "meta_vir.Rdata"))
```

## meta

```{r}
meta <- read_rds(here("data_cleaned", "meta_MUIS_m12.Rdata"))
```

# Add clusters

```{r clust_ind, w = 6, h = 2.8}
ps_clust <- ps$RA

sl(bc, vegdist(t(as(otu_table(ps_clust), "matrix")), method="bray"))
hc <- hclust(bc, "average")

sl(clust_ind, clust_ind_plot(bc, 50), overwrite = F)
clust_ind$plot # 8/14/24/31

k <- 31

sample_data(ps_clust) <- ps_clust %>%
  meta_to_df() %>%
  left_join(enframe(cutree(hc, k), name = "sample_id", value = "clust"), by = "sample_id") %>%
  column_to_rownames("sample_id")
#all(sample_names(ps_clust) == sample_names(ps$RA))
#all(hc$labels == sample_names(ps_clust))
```

# Add subcluster MOR-cluster

Further downstream, we found that the MOR-cluster is particularly large (_n_ = 587 OTUs) and based on heatmap inspection shows large variety (including CDG, MOR2 and MOR11 subclusters).
Increasing _k_ further did not resolve these, so we decided to run a subclustering on the samples in this MOR-cluster (cluster number 4).

```{r}
MOR_ids <- ps_clust %>%
  meta_to_df() %>%
  filter(clust == "4") %>%
  pull(sample_id)

bc_MOR <- as.matrix(bc)[MOR_ids, MOR_ids] %>% as.dist()
hc_MOR <- hclust(bc_MOR, "average")

sl(clust_ind_MOR, clust_ind_plot(bc_MOR, 20), overwrite = F)
clust_ind_MOR$plot # 3/10

k_MOR = 3

ps_clust_MOR <- ps_clust

sample_data(ps_clust_MOR) <- ps_clust %>%
  meta_to_df() %>%
  left_join(enframe(cutree(hc_MOR, k_MOR), name = "sample_id", value = "clust_MOR"), by = "sample_id") %>%
  mutate(clust = if_else(!is.na(clust_MOR), glue("{clust}.{clust_MOR}"), clust %>% as_glue()),
         clust = fct_lump_min(factor(clust), 10),
         clust = fct_recode(clust, STR14 = "2", CDG21 = "3", CDG5 = "4.1", MOR2 = "4.2", MOR11 = "4.3", HAE = "5", STA = "6", LAC = "10", STR13 = "16", NEI = "24") %>% fct_relevel(., c("STA", "LAC", "STR14", "CDG21", "CDG5", "MOR11", "MOR2", "HAE", "NEI", "STR13", "Other"))) %>%
  select(-clust_MOR) %>%
  column_to_rownames("sample_id")
```

# Add (optional) subcluster CDG5
```{r}
CDG5_ids <- ps_clust_MOR %>%
  meta_to_df() %>%
  filter(clust == "CDG5") %>%
  pull(sample_id)

bc_CDG5 <- as.matrix(bc)[CDG5_ids, CDG5_ids] %>% as.dist()
hc_CDG5 <- hclust(bc_CDG5, "average")

ps_clust_CDG5 <- ps_clust_MOR
sample_data(ps_clust_CDG5) <- ps_clust_MOR %>%
  meta_to_df() %>%
  left_join(enframe(cutree(hc_CDG5, h = 0.51), name = "sample_id", value = "clust_CDG5"),
            by = "sample_id") %>%
  mutate(clust = fct_expand(clust, "CDG5/MOR2")) %>%
  mutate(clust_CDG5 = replace(clust, clust_CDG5 == 2, "CDG5/MOR2")) %>%
  mutate(clust_CDG5 = fct_relevel(clust_CDG5, "CDG5/MOR2", after = 5)) %>%
  droplevels() %>%
  column_to_rownames("sample_id")
```

Note: cluster annotations added after inspection of profiles.

# Combine

## `ma` and `mb`

```{r}
meta_mb <- ps_clust_CDG5 %>% meta_to_df()

n_in_ma_not_mb <- setdiff(meta_ma$sample_id, meta_mb$sample_id) %>% length
n_in_mb_not_ma <- setdiff(meta_mb$sample_id, meta_ma$sample_id) %>% length

in_ma_not_mb <- anti_join(meta_ma, meta_mb, by="sample_id")

in_ma_not_mb %>% 
  count(subject)
```

`meta_ma` consists of `r nrow(meta_ma)` samples. `meta_mb` consists of `r f(nrow(meta_mb))` samples.
`r n_in_ma_not_mb` are available in `meta_ma` and not in `meta_mb` (these samples originate from `r in_ma_not_mb %>% pull(subject) %>% unique %>% length()` subjects).

```{r}
meta_mb_ma <- full_join(
  meta_ma %>%
    select(sample_id, filename, RIN, RIN_fac, ma_batch = MA_batch),
  meta_mb %>%
    select(sample_id, qpcr_16S_pgul, qpcr_16S_ct, pico_ngul, miseq_run_nr, clust_Bosch, in_Bosch, clust, clust_CDG5), 
  by = "sample_id") %>%
  left_join(., meta, by = "sample_id") %>%
  relocate(subject:ab_yn, .before = "filename") %>%
  arrange(subject, time)
```

### add viral data

```{r}
setdiff(meta_mb_ma$sample_id, meta_vir$sample_id) #3 not in _vir, available in _mb_ma
setdiff(meta_vir$sample_id, meta_mb_ma$sample_id) %>% length() #143 in _vir, not in _mb_ma

meta_mb_ma_vir <- left_join(
  meta_mb_ma,
  meta_vir %>% 
    select(-subject, -time_V, -time_V_RI, -time, -age_exact_visit),
  by="sample_id") #1169

meta_mb_ma_vir_Ct <- left_join(
  meta_mb_ma,
  meta_vir_Ct %>%
    select(sample_id, ends_with("_Ct")),
  by="sample_id") #1169
```

- 121150010292: geen sample meer van; niet geisoleerd (zie `MUIS NP virale DATABASE_bijgewerkt.xlsx`)
- 121140010692: was no more aliquot (zie `MUIS NP virale DATABASE_bijgewerkt.xlsx`)
- 121141010872: not in freezer list (zie `MUIS NP virale DATABASE_bijgewerkt.xlsx`)

# Split

## `_ma`

```{r}
#meta_mb_ma_vir #1192
#meta_vir #1309

meta_ma_final <- meta_mb_ma_vir %>% 
  filter(!is.na(filename)) %>%
  slice(match(colnames(expr), sample_id)) %>%
  droplevels #286

all(meta_ma_final$sample_id==colnames(expr))
```

Note: original `meta_ma` is overwritten here.

## `_mb`

```{r}
meta_mb_final <- meta_mb_ma_vir %>% #1156
  slice(match(sample_names(ps$raw), sample_id))

all(sample_names(ps$raw) == meta_mb_final$sample_id)

ps_add_meta <- function(psdata) {
  sample_data(psdata) <- meta_mb_final %>% column_to_rownames("sample_id")
  return(psdata)
}

ps <- map(ps, ps_add_meta)
```

# Save
```{r}
meta_ma <- meta_ma_final %>% droplevels
meta_mb <- meta_mb_final %>% droplevels

save(list=c("meta_mb", "ps",
            "meta_ma", "expr",
            "meta_mb_ma_vir", "meta_mb_ma_vir_Ct", "meta_vir", "meta_vir_woRI", "meta_vir_Ct"), 
     file=here("data_cleaned", "env_comb.Rdata"),
     envir=environment())
```

PM: add _wRI

# Session info

```{r session}
sessionInfo()
```
